import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsComponent } from './posts/posts.component';
import { PostFormComponent } from './post-form/post-form.component';
import { ViewPostsComponent } from './view-posts/view-posts.component';


const routes: Routes = [
  {path:"posts",component:ViewPostsComponent},
  {path:"addPost",component:PostFormComponent},
  {path:"",component:ViewPostsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
