import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../models/post.module';
import { PostService } from '../services/post-service.service';

@Component({
  selector: 'app-view-posts',
  templateUrl: './view-posts.component.html',
  styleUrls: ['./view-posts.component.css']
})
export class ViewPostsComponent implements OnInit,OnDestroy  {

  posts :Post[];
  constructor(private postService:PostService){
   
   
    this.posts=postService.posts;
  
  }

  ngOnInit() {
  }
  ngOnDestroy(){
    this.postService.savePostsToServer();
  }

}
