import { Component, OnInit, Input } from '@angular/core';
import { getCurrencySymbol } from '@angular/common';
import { PostService } from '../services/post-service.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  @Input() postnumber:String;
  @Input() datenumber:Date;
  @Input() paragraphe:String;
  @Input() lovestatus:number;
  @Input() dislovestatus:number;
  @Input() index: number;

   
  constructor( private postService:PostService) { }

  ngOnInit() {
  }

  increment(){
    this.lovestatus++;
    console.log("boutton like it:",this.lovestatus);
    this.postService.incrementlove(this.index);
  }
  
  decrement(){
    this.dislovestatus++;
    console.log("boutton dislike it:",this.dislovestatus);
    this.postService.incrementDislove(this.index)
  }
  onDeletePost(){
this.postService.deletePost(this.index);
  }

}
