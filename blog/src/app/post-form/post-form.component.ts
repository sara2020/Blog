import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { PostService } from '../services/post-service.service';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

  constructor(private postService:PostService,private router:Router
    ) { this.postService.savePostsToServer()}

  ngOnInit() {
  }
onSubmit(form:NgForm){
  const title=form.value['title'];
 const content=form.value['content'];
 this.postService.addPost(title,content);
 this.router.navigate(['posts']);

}
}
