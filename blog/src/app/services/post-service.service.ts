import { Injectable } from '@angular/core';
import { Post } from '../models/post.module';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  date:Date=new Date();
posts:Post[];
// [{  title: 'post 1',
//   content: 'post 1',
//   loveIts: 2,
//   created_at: this.date,
//   dontloveit:2,}]
  constructor(private httpClient:HttpClient) { 
    
  }
  
  
  addPost(title:string,content:string){
  
const postObject={
title:'',
content:'',
loveIts: 1,
  created_at: this.date,
  dontloveit:1
};
postObject.title=title;
postObject.content=content;
console.log('posts')
this.posts.push(postObject);
  }
  incrementlove(i:number){
    this.posts[i].loveIts+=1;

  }
  incrementDislove(i:number){
this.posts[i].dontloveit+=1 ;
  }
  savePostsToServer() {
    this.httpClient
      .put('https://post-b7592.firebaseio.com/posts.json', this.posts)
      .subscribe(
        () => {
          console.log('Enregistrement terminé !');
        },
        (error) => {
          console.log('Erreur ! : ' + error);
        }
      );
}
getPostsFromServer() {
  this.httpClient
    .get<any[]>('https://post-b7592.firebaseio.com/posts.json')
    .subscribe(
      (response) => {
        this.posts = response;
    
      },
      (error) => {
        console.log('Erreur ! : ' + error);
      },
      ()=>{
        console.log('get est termine')
      }
    );
}
deletePost(i:number){
  this.posts.splice(i,1);
console.log(this.posts)
 
}
}
