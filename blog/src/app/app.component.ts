import { Component, OnInit } from '@angular/core';
import { PostService } from './services/post-service.service';
import { Post } from './models/post.module';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
 
constructor(private postService:PostService){
  this.postService.getPostsFromServer();
}
ngOnInit(){

}
}
